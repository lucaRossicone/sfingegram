package asw.sfingegram.enigmiseguiti.eventlistner;

import asw.sfingegram.api_event.common.DomainEvent;
import asw.sfingegram.api_event.connessioni.ConnessioneConAutoreCreatedEvent;
import asw.sfingegram.api_event.connessioni.ConnessioneConTipoCreatedEvent;
import asw.sfingegram.api_event.enigmi.EnigmaCreatedEvent;
import asw.sfingegram.enigmiseguiti.connessioni.ConnessioneConAutore;
import asw.sfingegram.enigmiseguiti.connessioni.ConnessioneConTipo;
import asw.sfingegram.enigmiseguiti.connessioni.ConnessioniService;
import asw.sfingegram.enigmiseguiti.domain.EnigmiSeguitiService;
import asw.sfingegram.enigmiseguiti.enigmi.Enigma;
import asw.sfingegram.enigmiseguiti.enigmi.EnigmiService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.logging.Logger;

public class EventHandler implements IEventHandler {

    private final Logger logger = Logger.getLogger(EventHandler.class.toString());

    @Autowired
    private ConnessioniService connessioniService;
    @Autowired
    private EnigmiService enigmiService;
    @Autowired
    private EnigmiSeguitiService enigmiSeguitiService;

    public void onEventRaised(DomainEvent event){

        logger.info("===========================================================\n\n" +
                "PROCESSING EVENT: \n" + event +
                "\n\n=============================================================");

        if(event.getClass() == EnigmaCreatedEvent.class){
            EnigmaCreatedEvent enigmaEvent = (EnigmaCreatedEvent) event;
            Enigma enigma = this.enigmiService.addEnigma(enigmaEvent.getId(), enigmaEvent.getAutore(), enigmaEvent.getTipo(), enigmaEvent.getTitolo(), enigmaEvent.getTesto(), enigmaEvent.getSoluzione());
            this.enigmiSeguitiService.onEnigmaAdded(enigma);

        }else if(event.getClass() == ConnessioneConAutoreCreatedEvent.class){
            ConnessioneConAutoreCreatedEvent connEvent = (ConnessioneConAutoreCreatedEvent) event;
            ConnessioneConAutore conn = this.connessioniService.createConnessioneConAutore(connEvent.getId(), connEvent.getUtente(), connEvent.getAutore());
            this.enigmiSeguitiService.onConnessioneConAutoreAdded(conn);

        }else if(event.getClass() == ConnessioneConTipoCreatedEvent.class){
            ConnessioneConTipoCreatedEvent connEvent = (ConnessioneConTipoCreatedEvent) event;
            ConnessioneConTipo conn = this.connessioniService.createConnessioneConTipo(connEvent.getId(), connEvent.getUtente(), connEvent.getTipo());
            this.enigmiSeguitiService.onConnessioneConTipoAdded(conn);

        }else{
            logger.info("UNKNOWN EVENT: " + event);
        }

    }
}
