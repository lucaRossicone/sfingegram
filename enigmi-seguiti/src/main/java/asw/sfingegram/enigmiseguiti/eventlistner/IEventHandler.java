package asw.sfingegram.enigmiseguiti.eventlistner;

import asw.sfingegram.api_event.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface IEventHandler {

    void onEventRaised(DomainEvent event);
}
