package asw.sfingegram.enigmiseguiti.enigmi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
public class EnigmiService {

	@Autowired
	private EnigmaRepository enigmiRepository;

	public Enigma addEnigma(Long Id, String autore, String tipo, String titolo, String[] testo, String[] soluzione) {
		Enigma enigma = new Enigma(Id, autore, tipo, titolo, testo, soluzione);
		enigma = enigmiRepository.save(enigma);
		return enigma;
	}

	public Collection<Enigma> getEnigmiByAutore(String autore) {
		Collection<Enigma> enigmi = enigmiRepository.findByAutore(autore);
		return enigmi;
	}

	public Collection<Enigma> getEnigmiByTipo(String tipo) {
		Collection<Enigma> enigmi = enigmiRepository.findByTipoStartingWith(tipo);
		return enigmi;
	}


}
