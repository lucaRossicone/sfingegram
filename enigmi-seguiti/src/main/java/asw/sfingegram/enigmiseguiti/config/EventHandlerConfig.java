package asw.sfingegram.enigmiseguiti.config;

import asw.sfingegram.enigmiseguiti.eventlistner.EventHandler;
import asw.sfingegram.enigmiseguiti.eventlistner.IEventHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventHandlerConfig {

    @Bean
    public IEventHandler getHandler(){
        return new EventHandler();
    }
}
