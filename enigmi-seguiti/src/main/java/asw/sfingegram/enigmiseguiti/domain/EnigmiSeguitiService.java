package asw.sfingegram.enigmiseguiti.domain;

import asw.sfingegram.enigmiseguiti.connessioni.ConnessioneConAutore;
import asw.sfingegram.enigmiseguiti.connessioni.ConnessioneConTipo;
import asw.sfingegram.enigmiseguiti.connessioni.ConnessioniService;
import asw.sfingegram.enigmiseguiti.enigmi.Enigma;
import asw.sfingegram.enigmiseguiti.enigmi.EnigmiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Service 
public class EnigmiSeguitiService {

	@Autowired 
	private ConnessioniService connessioniService;

	@Autowired 
	private EnigmiService enigmiService;

	@Autowired
	private EnigmiSeguitiRepository enigmiSeguitiRepository;

	/* Trova gli enigmi (in formato breve) degli utenti seguiti da utente. */ 
	public Collection<Enigma> getEnigmiSeguiti(String utente) {
		Collection<EnigmaSeguito> enigmiSeguiti = this.enigmiSeguitiRepository.findByUtente(utente);
		Collection<Enigma> enigmi =
				enigmiSeguiti.
						stream().
						map(e -> new Enigma(e.IdEnigma, e.autoreEnigma, e.tipoEnigma, e.titoloEnigma, e.testoEnigma, e.soluzione)).
						collect(Collectors.toSet());
		return enigmi; 
	}

	public void onEnigmaAdded(Enigma enigma) {
		Collection<EnigmaSeguito> newSeguitiSet = new TreeSet<>();
		Collection<ConnessioneConAutore> connessioniPerAutore = this.connessioniService.getConnessioniConAutoriByAutore(enigma.getAutore());
		for(ConnessioneConAutore c : connessioniPerAutore){
			EnigmaSeguito e = new EnigmaSeguito(c.getUtente(), enigma);
			newSeguitiSet.add(e);
		}
		Collection<ConnessioneConTipo> connessioniPerTipo = this.connessioniService.getConnessioniConTipoByTipo(enigma.getTipo());
		for(ConnessioneConTipo c : connessioniPerTipo){
			EnigmaSeguito e = new EnigmaSeguito(c.getUtente(), enigma);
			newSeguitiSet.add(e);
		}
		this.enigmiSeguitiRepository.saveAll(newSeguitiSet);
	}

	public void onConnessioneConAutoreAdded(ConnessioneConAutore conn) {
		Collection<Enigma> enigmi = this.enigmiService.getEnigmiByAutore(conn.getAutore());
		Collection<EnigmaSeguito> newEnigmiSeguiti =
				enigmi.stream()
				.map(e -> new EnigmaSeguito(conn.getUtente(), e))
				.collect(Collectors.toSet());
		this.enigmiSeguitiRepository.saveAll(newEnigmiSeguiti);
	}

	public void onConnessioneConTipoAdded(ConnessioneConTipo conn){
		Collection<Enigma> enigmi = this.enigmiService.getEnigmiByAutore(conn.getTipo());
		Collection<EnigmaSeguito> newEnigmiSeguiti =
				enigmi.stream()
						.map(e -> new EnigmaSeguito(conn.getUtente(), e))
						.collect(Collectors.toSet());
		this.enigmiSeguitiRepository.saveAll(newEnigmiSeguiti);
	}



}
