package asw.sfingegram.enigmiseguiti.domain;

import asw.sfingegram.enigmiseguiti.enigmi.Enigma;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.protocol.types.Field;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@IdClass(EnigmaSeguitoId.class)
public class EnigmaSeguito {

    @Id
    @EqualsAndHashCode.Include
    String utente;
    @Id
    @EqualsAndHashCode.Include
    Long IdEnigma;
    String autoreEnigma;
    String tipoEnigma;
    String titoloEnigma;
    String[] testoEnigma;
    String[] soluzione;

    public EnigmaSeguito(String utente, Enigma enigma){
        this(utente, enigma.getId(), enigma.getAutore(), enigma.getTipo(), enigma.getTitolo(), enigma.getTesto(), enigma.getSoluzione());
    }
}
