package asw.sfingegram.enigmiseguiti.connessioni;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Collection;

@Service 
@Primary 
public class ConnessioniService {

	@Autowired
    private ConnessioneConAutoreRepository connessioneConAutoreRepository;
	@Autowired
    private ConnessioneConTipoRepository connessioneConTipoRepository;

	public ConnessioneConAutore createConnessioneConAutore(Long Id, String utente, String autore){
		ConnessioneConAutore conn = new ConnessioneConAutore(Id, utente, autore);
		this.connessioneConAutoreRepository.save(conn);
		return conn;
	}

	public ConnessioneConTipo createConnessioneConTipo(Long Id, String utente, String tipo){
		ConnessioneConTipo conn = new ConnessioneConTipo(Id, utente, tipo);
		this.connessioneConTipoRepository.save(conn);
		return conn;
	}

	public Collection<ConnessioneConAutore> getConnessioniConAutoriByAutore(String autore){
	    return this.connessioneConAutoreRepository.findByAutore(autore);
    }

    public Collection<ConnessioneConTipo> getConnessioniConTipoByTipo(String tipo){
	    return this.connessioneConTipoRepository.findByTipo(tipo);
    }

	public Collection<ConnessioneConAutore> getConnessioniConAutoriByUtente(String utente) {
		return this.connessioneConAutoreRepository.findByUtente(utente);
	}	

	public Collection<ConnessioneConTipo> getConnessioniConTipiByUtente(String utente) {
		return this.connessioneConTipoRepository.findByUtente(utente);
	}	

}
