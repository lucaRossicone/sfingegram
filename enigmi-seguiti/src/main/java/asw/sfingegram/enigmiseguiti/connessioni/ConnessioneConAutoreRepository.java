package asw.sfingegram.enigmiseguiti.connessioni;

import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface ConnessioneConAutoreRepository extends CrudRepository<ConnessioneConAutore, Long> {

    public Collection<ConnessioneConAutore> findByUtente(String utente);
    public Collection<ConnessioneConAutore> findByAutore(String autore);

}
