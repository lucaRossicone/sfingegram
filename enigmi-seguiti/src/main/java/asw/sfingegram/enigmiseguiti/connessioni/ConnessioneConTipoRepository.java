package asw.sfingegram.enigmiseguiti.connessioni;


import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface ConnessioneConTipoRepository extends CrudRepository<ConnessioneConTipo, Long> {

    public Collection<ConnessioneConTipo> findByUtente(String utente);
    public Collection<ConnessioneConTipo> findByTipo(String tipo);
}
