package asw.sfingegram.api_event.connessioni;

import asw.sfingegram.api_event.common.DomainEvent;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ConnessioneConAutoreCreatedEvent implements DomainEvent {
    @EqualsAndHashCode.Include
    Long Id;
    String utente;
    String autore;

}
