package asw.sfingegram.api_event.connessioni;

import asw.sfingegram.api_event.common.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ConnessioneConTipoCreatedEvent implements DomainEvent {

    @EqualsAndHashCode.Include
    Long Id;
    String utente;
    String tipo;
}
