package asw.sfingegram.connessioni.domain;

import asw.sfingegram.api_event.common.DomainEvent;
import lombok.*;

public interface ConnessioniEventPublisher {

    public void publish(DomainEvent event);
}

