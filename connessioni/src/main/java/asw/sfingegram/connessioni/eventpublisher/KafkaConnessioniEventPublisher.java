package asw.sfingegram.connessioni.eventpublisher;

import java.util.logging.Logger;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import asw.sfingegram.api_event.common.DomainEvent;
import asw.sfingegram.api_event.connessioni.ConnessioniServiceEventChannel;
import asw.sfingegram.connessioni.domain.ConnessioniEventPublisher;
import java.util.logging.Logger;

@Component
public class KafkaConnessioniEventPublisher implements ConnessioniEventPublisher {

    @Autowired
    private KafkaTemplate<String, DomainEvent> template;
    private final Logger logger = Logger.getLogger(KafkaConnessioniEventPublisher.class.toString());
    private String channel = ConnessioniServiceEventChannel.channel;

    @Override
    public void publish(DomainEvent event) {
        logger.info("PUBLISHING MESSAGE: " + event + " ON CHANNEL: " + channel);
        template.send(channel, event);
    }
}
