package asw.sfingegram.enigmi.domain;

import asw.sfingegram.api_event.common.DomainEvent;
import asw.sfingegram.api_event.enigmi.EnigmaCreatedEvent;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.*;


@Service
public class EnigmiService {

	@Autowired
	private EnigmiRepository enigmiRepository;

	@Autowired
	private EnigmiEventPublisher enigmiEventPublisher;

 	public Enigma createEnigma(String autore, String tipo, String titolo, String[] testo, String[] soluzione) {
		Enigma enigma = new Enigma(autore, tipo, titolo, testo, soluzione); 
		enigma = enigmiRepository.save(enigma);
		DomainEvent event = new EnigmaCreatedEvent(enigma.getId(), enigma.getAutore(), enigma.getTipo(), enigma.getTitolo(), enigma.getTesto(), enigma.getSoluzione());
		enigmiEventPublisher.publish(event);
		return enigma;
	}

 	public Enigma getEnigma(Long id) {
		Enigma enigma = enigmiRepository.findById(id).orElse(null);
		return enigma;
	}

	public Collection<Enigma> getEnigmi() {
		Collection<Enigma> enigmi = enigmiRepository.findAll();
		return enigmi;
	}

	public Collection<Enigma> getEnigmiByAutore(String autore) {
		Collection<Enigma> enigmi = enigmiRepository.findByAutore(autore);
		return enigmi;
	}

	public Collection<Enigma> getEnigmiByAutori(Collection<String> autori) {
		Collection<Enigma> enigmi = enigmiRepository.findByAutoreIn(autori);
		return enigmi;
	}

	public Collection<Enigma> getEnigmiByTipo(String tipo) {
		Collection<Enigma> enigmi = enigmiRepository.findByTipoStartingWith(tipo);
		return enigmi;
	}

	public Collection<Enigma> getEnigmiByTipi(Collection<String> tipi) {
		Collection<Enigma> enigmi = 
			tipi
				.stream()
				.flatMap(t -> enigmiRepository.findByTipoStartingWith(t).stream())
				.collect(Collectors.toSet()); 
		return enigmi;
	}

}
