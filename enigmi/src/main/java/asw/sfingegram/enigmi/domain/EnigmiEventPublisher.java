package asw.sfingegram.enigmi.domain;
import asw.sfingegram.api_event.common.DomainEvent;
import lombok.*;

public interface EnigmiEventPublisher {

    public void publish(DomainEvent event);
}
